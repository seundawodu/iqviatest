import random
import datetime
import os

from celery import Celery
from celery.schedules import crontab
from app.main import create_app


c_app = Celery('tasks', broker='redis://localhost:6379/0')
app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')

from app.main.service.contact_service import (
    create_contact,
    delete_contact,
    list_contacts
)

@c_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):


    sender.add_periodic_task(15, generate_random_contact.s())
    sender.add_periodic_task(15, clear_old_contacts.s())

def gen_random_str():
    chars = 'abcdefghijklmnopqrs'
    length = random.randint(1, 10)
    return ''.join(random.choices(chars, k=length))

@c_app.task
def generate_random_contact():
    print("Creating new contact")
    firstname = gen_random_str()
    surname = gen_random_str()
    username = '{0}_{1}'.format(firstname, surname)
    email0 = '{0}@{1}.com'.format(firstname, surname)
    email1 = '{0}@{1}.com'.format(surname, firstname)

    with app.app_context():
        contact = create_contact(
            {
                'firstname': firstname,
                'surname': surname,
                'username': username,
                'emails': [email0, email1]
            }
        )
        print("Contact: %s" % contact)


@c_app.task
def clear_old_contacts():
    print("Deleting old contacts")
    now = datetime.datetime.now()
    with app.app_context():
        contacts_to_delete = [
            i.username for i in list_contacts()
            # Handle contacts created before created field introduced
            if i.created is None  or (now - i.created).seconds > 60
        ]
        print ("Deleting %s contacts" % len(contacts_to_delete))

        for i in contacts_to_delete:
            delete_contact(i)
