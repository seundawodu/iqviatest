# IQVIA development test

## Set up

From root of repo

1. Create python3 virtual env
2. Activate virtual env
3. Install requirements from requirements.txt
```
python manage.py db upgrade
```

## Tests

```
python manage.py test
```

## To run

```
python manage.py run
```

## To start celery worker for periodic tasks
```
 celery -A tasks.c_app worker -B --loglevel=info
```
