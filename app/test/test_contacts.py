import unittest
import datetime
import random

from app.main import db
from app.main.model.contact import Contact, Email
from app.test.base import BaseTestCase


class ContactTest(BaseTestCase):
    

    def add_contact(
            self,
            firstname='Testfname',
            surname='Surname',
            username='thefirst',
            emails=None
    ):
        if emails is None:
            emails = ['me@thefirst.com']
        contact = Contact(
            firstname=firstname,
            surname=surname,
            username=username,
        )
        db.session.add(contact)
        for email in emails:
            email = Email(email_address=email)
            db.session.add(email)
            contact.emails.append(email)
        db.session.commit()
        return contact


    def test_get_by_username_retrieves_correct(self):
        # arrange
        contact = self.add_contact()
        
        # act
        contact_data = self.client.get('/contact/thefirst')

        # assert
        self.assertEqual(contact_data.json.get('id'), contact.id)

    def test_get_by_email_address(self):
        contact_0 = self.add_contact()
        contact_1 = self.add_contact(surname='Another', username='thesecond', emails=['me@thesecond.com'])

        contact_data = self.client.get('/contacts/?email=me@thesecond.com')
        print(contact_data.json)

        self.assertEqual(contact_data.json.get('id'), contact_1.id)

    def test_get_invalid_username_returns_404(self):

        # act
        contact_data = self.client.get('/contact/doesntexist')

        self.assertEqual(contact_data._status_code, 404)

    def test_get_list_returns_all_items(self):
        num_contacts = random.randint(1,10)
        for i in range(num_contacts):
            contact = self.add_contact(
                firstname='Testfname{0}'.format(i),
                surname='Surname{0}'.format(i),
                username='thefirst{0}'.format(i),
                emails=['me{0}@thefirst.com'.format(i)]
            )

        contacts = self.client.get('/contacts/')

        self.assertEqual(len(contacts.json), num_contacts)

    def test_post_creates_new_item(self):

        contacts =  self.client.get('/contacts/')
        self.assertEqual(len(contacts.json), 0)

        self.client.post(
            '/contacts/',
            json={
                'firstname': 'Testfname',
                'surname': 'Surname',
                'username': 'thefirst',
                'emails': ['me@thefirst.com']
            }
        )
        contacts =  self.client.get('/contacts/')
        self.assertEqual(len(contacts.json), 1)    

    def test_put_updates_item(self):
        # arrange
        self.add_contact()
        
        contact = self.client.put(
            '/contact/thefirst',
            json={
                'firstname': 'ChangedName'
            }
        )

        self.assertEqual(contact.json.get('firstname'), 'ChangedName')

    def test_put_updates_multiple_email_addresses(self):
        orig_contact = self.add_contact(emails=['1@example.com', '2@example.com'])

        contact = self.client.put(
            '/contact/{0}'.format(orig_contact.username),
            json={
                'emails': ['3@example.com', '4@example.com']
            }
        )

        self.assertEqual(set(contact.json.get('emails')), set(['3@example.com', '4@example.com']))

    def test_delete_removes_item(self):

        # arrange
        self.add_contact()

        self.client.delete('/contact/thefirst')

        contacts =  self.client.get('/contacts/')
        self.assertEqual(len(contacts.json), 0)

    def test_delete_removes_orphan_emails(self):

        orig_contact = self.add_contact(emails=['1@example.com', '2@example.com'])

        self.client.delete(
            '/contact/{0}'.format(orig_contact.username)
        )

        remaining_emails = Email.query.order_by(Email.email_address).all()
        self.assertEqual(len(remaining_emails), 0)
if __name__ == '__main__':
    unittest.main()