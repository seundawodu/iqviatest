from app.main import db
from app.main.model.contact import Contact, Email


def update_contact(contact_name, params):
    contact = get_contact(contact_name)

    firstname = params.get('firstname') or contact.firstname
    surname = params.get('surname') or contact.surname
    username = params.get('username') or contact.username

    requested_emails = set(params.get('emails', []))
    existing_emails = set([i.email_address for i in contact.emails])

    to_add = requested_emails.difference(existing_emails)
    to_remove = existing_emails.difference(requested_emails)
    try:
        contact.firstname = firstname
        contact.surname = surname
        contact.username = username
        db.session.add(contact)
        for email in to_add:
            email =  Email(email_address=email)
            db.session.add(email)
            contact.emails.append(email)

        for email in to_remove:
            r_email = Email.query.filter(Email.email_address==email).first()
            contact.emails.remove(r_email)

        db.session.commit()
    except:
        db.session.rollback()
        raise

    return contact

def create_contact(params):
    try:
        emails = params.get('emails')
        del(params['emails'])
        contact = Contact(**params)
        db.session.add(contact)
        for email in emails:
            email = Email(email_address=email)
            db.session.add(email)
            contact.emails.append(email)
        db.session.commit()
    except:
        db.session.rollback()
        raise
    return contact

def get_contact(contact_name):
    return Contact.query.filter(
        Contact.username==contact_name
    ).first_or_404()   
def get_contact_by_email(email):
    email_obj = Email.query.filter(Email.email_address==email).first_or_404()
    contact = Contact.query.filter(Contact.id==email_obj.person_id).first_or_404()
    return contact
def delete_contact(contact_name):
    db.session.delete(get_contact(contact_name))
    db.session.commit()

def list_contacts():
    return Contact.query.all()