from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from app.main.service.contact_service import (
    get_contact,
    get_contact_by_email,
    list_contacts,
    delete_contact,
    create_contact,
    update_contact
)


class ContactDetail(Resource):

    def get(self, contact_username):
        
        return jsonify(
            get_contact(contact_username    ).serialise
        )

    def put(self,contact_username):

        contact = update_contact(contact_username, request.json)

        return jsonify(contact.serialise)

    def delete(self, contact_username):
        delete_contact(contact_username)

class ContactList(Resource):

    def get(self):
        if 'email' in request.args:
            contact =get_contact_by_email(request.args.get('email'))
            return contact.serialise

        return jsonify(
            [contact.serialise for contact in list_contacts()]
        )

    def post(self, contact_id=None):
        contact = create_contact(
            request.json
        )

        return jsonify(contact.serialise)