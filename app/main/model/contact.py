import datetime
from .. import db


class Contact(db.Model):
    
    __tablename__ = "contact"

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    username = db.Column(db.String(255), nullable=False, unique=True)
    firstname = db.Column(db.String(255), nullable=False)
    surname = db.Column(db.String(255), nullable=False)
    emails = db.relationship(
        'Email',
        backref='contact',
        lazy=True,
        cascade='all, delete-orphan'
    )

    @property
    def serialise(self):
        return {
            'id': self.id,
            'emails': [i.email_address for i in self.emails],
            'username': self.username,
            'firstname': self.firstname,
            'surname': self.surname,
            'created': self.created.strftime("%Y-%m-%d %H:%M:%S")
        }

class Email(db.Model):

    __tablename__ = "email"

    id = db.Column(db.Integer, primary_key=True)
    email_address = db.Column(db.String(255), nullable=False, unique=True)
    person_id = db.Column(
        db.Integer,
        db.ForeignKey('contact.id'),
        nullable=False
    )
